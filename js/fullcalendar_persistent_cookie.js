(function ($) {
  'use strict';

  if (typeof Drupal.fullcalendar !== 'undefined') {
    Drupal.fullcalendar.plugins.fullcalendar_persistent = {
      options: function (fullcalendar, settings) {
        var options = {
          viewRender: function (view, element) {
            // Set the DefaultView for future viewings...
            $.cookie('STYXKEY_calendarDefaultView', view.name, {expires: 7, path: '/'});
            // Set the starting date for future viewings...
            $.cookie('STYXKEY_calendarCurrentDate', view.start.getFullYear() + '-' + view.start.getMonth() + '-' + view.start.getDate(), {expires: 7, path: '/'});
          }
        };
        return options;
      }
    };
  }

})(jQuery);
